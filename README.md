# House Price Prediction

This is the implementation of https://doi.org/10.1007/s10618-018-00612-0 paper. In the paper, the authors propose a new model to predict house prices that outperform previously algorithms based on KNN, ANN, LR.